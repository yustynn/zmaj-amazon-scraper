const TIMEOUT = 100
const GIVE_UP_TIME = 5000

function getCandidateEls(rootNode) {
    return Array.from(rootNode.querySelectorAll("#productDetails_db_sections tr")).concat(
        Array.from(rootNode.querySelectorAll("#detailBulletsWrapper_feature_div .a-list-item"))
    )
}

function begin(numTries) {
    // Keeps retrying until gives up

    if (!numTries) numTries = 0
    if (numTries > GIVE_UP_TIME/TIMEOUT) {
        console.log('[Zmaj Amazon] Could not find BSR.')
        return
    }
    const els = getCandidateEls(document)
    if (els.length == 0) {
        window.setTimeout(() => begin(numTries+1), TIMEOUT)
        return
    }

    populateZmajBar(createZmajBar(document.querySelector('#navbar')))
}

function populateZmajBar(elDisplay) {
    const el = getCandidateEls(document).filter(tr => tr.textContent.includes("Best Sellers Rank"))[0]
    
    let text = el.textContent.replace(/\(.*\)/, '')
    const hrefs = Array.from(el.querySelectorAll('a')).map(a => a.href)
    
    const getMatch = (text) => text.match(/([\d,]+) in (.+?)\s*(\d|$)/)

    let match = getMatch(text)
    let heightPx = 20
    i = 0
    while (match) {
        let num = match[1]
        const cat = match[2]
        text = text.replace(num, '').replace(cat, '') // make room for next match.

        const elNum = document = document.createElement('span')
        elNum.className = 'zmaj-num'
        elNum.textContent = num

        const elIn = document = document.createElement('span')
        elIn.textContent = ' in '

        const elCat = document = document.createElement('a')
        elCat.className = 'zmaj-cat'
        elCat.textContent = cat
        elCat.href = hrefs[i]

        const p = elDisplay.appendChild(document.createElement('p'))
        p.appendChild(elNum)
        p.appendChild(elIn)
        p.appendChild(elCat)

        match = getMatch(text)
        i += 1
        heightPx += 25
    }
    elDisplay.style.height = heightPx+'px'
}

function createZmajBar(parent) {
    const elZmajBar = document.createElement('div')
    elZmajBar.className = 'zmaj-bar'
    parent.append(elZmajBar)

    return elZmajBar
}

begin()