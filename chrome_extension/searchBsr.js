function addBsrs() {
    const SELECTOR_ITEM = "[data-component-type='s-search-result']"
    const SELECTOR_CARD = ".s-card-container"
    const SELECTOR_IMAGE = ".s-product-image-container"

    function select(selector, base) {
        if (!base) base = document
        return Array.from(base.querySelectorAll(selector))
    }

    function begin() {
        const elItems = select(SELECTOR_ITEM)
        elItems.forEach(getBsr)
    }

    function getCandidateEls(rootNode) {
        return Array.from(rootNode.querySelectorAll("#productDetails_db_sections tr")).concat(
            Array.from(rootNode.querySelectorAll("#detailBulletsWrapper_feature_div .a-list-item"))
        )
    }

    function populateZmajBar(elDisplay, rootNode) {
        const el = getCandidateEls(rootNode || document).filter(tr => tr.textContent.includes("Best Sellers Rank"))[0]

        let text = el.textContent.replace(/\(.*\)/, '')
        const hrefs = Array.from(el.querySelectorAll('a')).map(a => a.href)

        const getMatch = (text) => text.match(/([\d,]+) in (.+?)\s*(\d|$)/)

        let match = getMatch(text)
        let heightPx = 20
        i = 0
        while (match) {
            let num = match[1]
            const cat = match[2]
            text = text.replace(num, '').replace(cat, '') // make room for next match.

            const elNum = document = document.createElement('span')
            elNum.className = 'zmaj-num'
            elNum.textContent = num

            const elIn = document = document.createElement('span')
            elIn.textContent = ' in '

            const elCat = document = document.createElement('a')
            elCat.className = 'zmaj-cat'
            elCat.textContent = cat
            elCat.href = hrefs[i]

            const p = elDisplay.appendChild(document.createElement('p'))
            p.appendChild(elNum)
            p.appendChild(elIn)
            p.appendChild(elCat)

            match = getMatch(text)
            i += 1
            heightPx += 30
        }
        elDisplay.style.height = heightPx + 'px'
    }

    function getBsr(item) {
        const url = item.querySelector('a').href
        const elImg = item.querySelector(SELECTOR_CARD).querySelector(SELECTOR_IMAGE)
        const elZmajBar = document.createElement('div')
        elZmajBar.className = 'zmaj-bar'
        elImg.parentNode.insertBefore(elZmajBar, elImg)

        setTimeout(() => {
            fetch(url).then(res => res.text())
                .then((html) => {
                    var parser = new DOMParser();
                    var rootNode = parser.parseFromString(html, 'text/html');

                    populateZmajBar(elZmajBar, rootNode)
                })
        }, Math.random() * 5000)

    }

    begin()
}
  
chrome.action.onClicked.addListener((tab) => {
if (!tab.url.includes('chrome://')) {
    chrome.scripting.executeScript({
    target: { tabId: tab.id },
    function: addBsrs
    });
}
});