const SELECTOR_BAR = '[data-component-type=s-result-info-bar]'
  + ' [data-cel-widget="UPPER-RESULT_INFO_BAR-0"]'
  + ' .a-section.a-spacing-small.a-spacing-top-small'
const SELECTOR_ITEM = "[data-component-type='s-search-result']"
const SELECTOR_CARD = ".s-card-container"

function select(selector, base) {
  if (!base) base = document
  return Array.from(base.querySelectorAll(selector))
}

function begin() {
  const elItems = select(SELECTOR_ITEM)
  const elBar = select(SELECTOR_BAR)[0]
  if (!elItems.length || !elBar) {
    return window.setTimeout(begin, 500)
  }

  const prices = []
  let currency;
  for (const itemEl of elItems) {
    const raw = select('.a-price > .a-offscreen', itemEl)
    if (!raw.length) {
      prices.push(null)
      continue
    }
    const price = +raw[0].textContent.match(/[\d\.]+/)[0]
    prices.push(price)
    currency = raw[0].textContent.replace(price, "")
  }

  const relPrices = prices.filter(p => p)
  const avgPrice = (relPrices.reduce((a,b) => a + b, 0) / relPrices.length).toFixed(2)

  const span = document.createElement('span')
  span.textContent = ` |️ Average Price: ${currency}${avgPrice} ❤️ forzmaj gogogo ❤️`
  elBar.appendChild(span)
}

begin()