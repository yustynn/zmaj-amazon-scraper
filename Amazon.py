"""
For Zmaj

- Get num results for search term on amazon
- Get average price for search term on amazon
"""

from main import Scraper

from abc import ABC, abstractmethod
import re
from bs4 import BeautifulSoup
from typing import Any, Dict, List, Tuple

# constants
NUM_RESULTS_LOWER_BOUND = 'lower_bound'
NUM_RESULTS_EXACT = 'exact'

# types
ExtractorResult = Tuple[Any, Dict[str, Any]] # (result, additional_info)


class Extractor(ABC):
    regex: str
    selector: str

    @abstractmethod
    def run(self, soup: BeautifulSoup) -> ExtractorResult:
        pass


class ExtractorPrices(Extractor):
    regex = '\d+\.\d+'
    selector = "[data-component-type='s-search-result']"
    selector2 = ".a-price > .a-offscreen"

    def run(self, soup: BeautifulSoup) -> ExtractorResult:
        items = soup.select(self.selector)
        prices_raw = [s.text for s in soup.select(self.selector)]
        prices = []

        for item in items:
            price_els = item.select(self.selector2)
            if len(price_els) == 0:
                prices.append(None)
                continue

            price_raw = price_els[0].text
            price = float(re.search(self.regex, price_raw)[0])
            prices.append(price)

            # don't need to do this in the loop, but i'm lazy
            currency = price_raw.replace(str(price), '')


        return prices, {'currency': currency}


class ExtractorNumResults(Extractor):
    regex = '.+of (over )?([\d,]+)\s'
    selector = '[data-component-type=s-result-info-bar]'

    def run(self, soup: BeautifulSoup) -> ExtractorResult:
        raw = soup.select(self.selector)[0].text
        processed = int(re.search(self.regex, raw)[2].replace(',', ''))

        quantity_type = NUM_RESULTS_LOWER_BOUND if 'over' in raw else NUM_RESULTS_EXACT

        return processed, {'quantity_type': quantity_type}


class ExtractorNumReviews(Extractor):
    regex = 'stars ([\d,]+)'
    selector = "[data-component-type='s-search-result']"

    def run(self, soup: BeautifulSoup) -> ExtractorResult:
        raws = [s.text for s in soup.select(self.selector)]
        processeds = []

        for raw in raws:
            if not 'stars' in raw:
                processeds.append(0)
                continue
            num_reviews = int(re.search(self.regex, raw)[1].replace(',', ''))
            processeds.append(num_reviews)

        return processeds, {}

class Amazon:
    def __init__(self):
        self._scraper = Scraper()

    def run(self, search_term: str):
        self.search_term = search_term

        self._scraper.get(self._mk_url())
        self._scraper.wait_until_selector_loaded(ExtractorPrices.selector)

        raw_prices, self.prices_info = ExtractorPrices().run(self._scraper.soup)
        self.num_results, self.num_results_info = ExtractorNumResults().run(self._scraper.soup)
        raw_num_reviews, _ = ExtractorNumReviews().run(self._scraper.soup)

        # filter out unpriced things
        self.prices = [p for p in raw_prices if p]
        self.num_reviews = [nr for p, nr in zip(raw_prices, raw_num_reviews) if p]

        num_reviews = len(self.num_reviews) 
        num_prices = len(self.prices)
        assert num_reviews == num_prices, f"Mismatch between num reviews ({num_reviews}) and num prices ({num_prices})"

        self._display()


    def _mk_url(self) -> str:
        return f'https://www.amazon.com/s?k={self.search_term.replace(" ", "+")}'

    def _display(self):
        avg_price = round(sum(self.prices)/len(self.prices), 2)

        print(f'Search term: {self.search_term}')
        print(f'Avg price: {avg_price} (currency: {self.prices_info["currency"]})')
        print(f'Num priced items: {len(self.prices)}')
        print(f'Num results: {self.num_results} (quantity type: {self.num_results_info["quantity_type"]})')
        print(f'Num reviews: {self.num_reviews}')
        print('-'*20)

if __name__ == '__main__':
    a = Amazon()
    a.run('my stuff')