"""
Personal library to scrape stuff using Selenium
"""

# import asyncio

# from typing import Callable, List

from bs4 import BeautifulSoup
from selenium import webdriver
# from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.chrome.service import Service as ChromeService
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait

MAX_TIMEOUT = 180 # in seconds. Just in case 

class Scraper:
    def __init__(self):
        # adapted from https://www.zenrows.com/blog/selenium-python-web-scraping#add-real-headers
        user_agent = 'Mozilla/5.0 (Linux; Android 11; 100011886A Build/RP1A.200720.011) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.5112.69 Safari/537.36'
        sec_ch_ua = '"Google Chrome";v="104", " Not;A Brand";v="105", "Chromium";v="104"'
        referer = 'https://www.google.com'

        def interceptor(request):
            # delete the "User-Agent" header and
            # set a new one
            del request.headers["user-agent"]  # Delete the header first
            request.headers["user-agent"] = user_agent
            # set the "Sec-CH-UA" header
            request.headers["sec-ch-ua"] = sec_ch_ua
            # set the "referer" header
            request.headers["referer"] = referer

        options = Options()

        driver = webdriver.Chrome(
            options=options,
            service=ChromeService()
        )
        # set the Selenium Wire interceptor
        driver.request_interceptor = interceptor
        
        self.driver = driver


    def wait_until_selector_loaded(self, selector: str):
        """Returns soup"""

        def _check(driver):
            el = driver.find_element(By.CSS_SELECTOR, selector) 
                        
            return bool(el)
            
        WebDriverWait(self.driver, timeout=MAX_TIMEOUT).until(_check)

        return self.soup
    
    @property
    def soup(self) -> BeautifulSoup:
        """Returns soup"""
        return BeautifulSoup(self.driver.page_source, 'lxml')

    
    def get(self, url: str) -> BeautifulSoup:
        """Returns soup"""
        self.driver.get(url)

        return self.soup

    # @classmethod
    # async def parallel_run(cls, processing_fn: Callable[[WebDriver, List[str]], list], num_parallel: int, urls: List[str]) -> list:
    #     """
    #     processing_fn is async and takes in driver and list of urls
    #     """

    #     num_per_driver = len(urls) // num_parallel
    #     num_last_driver = len(urls) - num_per_driver * num_parallel

    #     tasks = []
    #     for i in range(num_parallel):
    #         if i < num_parallel - 1:
    #             task = processing_fn(cls(), urls[i*num_per_driver:(i+1)*num_per_driver])
    #         else:
    #             task = processing_fn(cls(), urls[i*num_per_driver:])
        

    #     results = []
    #     for res in await asyncio.gather(*tasks):
    #         results += res

    #     return results


