import streamlit as st

import plotly.express as px

from Amazon import Amazon, NUM_RESULTS_LOWER_BOUND

if 'amazon' not in st.session_state:
    st.session_state['amazon'] = Amazon()

amazon = st.session_state['amazon']

def retrieve_results(search_term: str):
    amazon.run(search_term)

    return amazon

def display_results():
    avg_price = round(sum(amazon.prices) / len(amazon.prices), 2)
    st.markdown(f'**Average Price**: {amazon.prices_info["currency"]}{avg_price}')
    is_lower_bound = amazon.num_results_info['quantity_type'] == NUM_RESULTS_LOWER_BOUND
    st.markdown(f'**Number of Results**: {">" if is_lower_bound else ""}{amazon.num_results}')

def display_price_chart():
    fig = px.histogram(
        x=amazon.prices,
        title='Distribution of Prices',
        labels={
            'x': 'Price Bucket',
            'y': 'Num Items',
        }
    )

    st.plotly_chart(fig)

def display_rank_num_reviews():
    fig = px.bar(
        x=list(range(1, len(amazon.prices)+1)),
        y=amazon.prices,
        color=amazon.num_reviews,
        title='Page Rank vs Price',
        labels={
            'x': 'Page Rank',
            'y': 'Price',
            'color': 'Num Reviews'
        }

    )

    st.plotly_chart(fig)

st.title("Zmaj's Amazon Tool")


search_term = st.text_input("Search term")
st.write("Search term:", search_term)
if st.button("Run"):
    results = retrieve_results(search_term)
    display_results()
    display_price_chart()
    display_rank_num_reviews()


